# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Barcelona Nightlife | Barcelona Clubs ###

* Quick summary
Barcelona Nightlife
Want to party the night away with some awesome Barcelona nightlife? Well, this page is going to give you a ton of information about what you need! We are not going to mention any specific locations on this page as the rest of the website is dedicated to individual night clubs and the like. We just want to give you a rough guide as to what you can expect when you are diving into Barcelona nightlife.
What do you want to do?
Whether you are looking for a [strip club Barcelona](https://www.stripclubbarcelona.com/es/), a night club, or something else to fill up your evenings while in Barcelona, you have plenty of choice!
Now, we are going to assume that you are travelling with a group. It is likely that all of you have different tastes when it comes to what to do of an evening. Sure, they may be some overlap, but what may be fun for one person may not necessarily be fun for another person. Therefore, when you are looking for Barcelona nightlife, we recommend that you think about what most people want. We are not just talking about whether you are heading to a night club or a strip club here. We are also talking about the type of music that you will be enjoying and all that jazz. Of course, if you are travelling as part of a stag group, and we know that many of the people that land on this website are, then make sure that you choose a night club that is interesting to the stag. They are the ones meant to be having a ton of fun!
Entrance Fees
Decent night clubs and [strip clubs Barcelona](https://stripclubsbarcelona.club/es/) will have an entrance fee. Sadly, there is not really a lot that you can do to get around this. There are a few exceptions to this rule. There are some that may offer free entrance if you are bringing a sizeable group along with you, but try not to count on this too much. 
It is probably worth noting that the night clubs and strip clubs with higher entrance fees tend to be a bit better. When the entrance fee is quite low, you tend to have a rather big �rabble� partying the night away. It can give you quite a headache! The more exclusive clubs tend to be very laidback and this is something that we genuinely love. Remember; the more expensive clubs tend to have cheaper drinks too.
Should you read reviews?
We always suggest that you dive into a few reviews when you are looking into Barcelona nightlife. This way you will get an idea as to what to expect when you are heading out and partying. However, you should probably remember that people tend to be more negative when they write reviews online. So, always do try to balance up the positive and the negative reviews to make sure that the nightclub is the right choice for you. The information you find on our website is a good starting point. 
�

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact